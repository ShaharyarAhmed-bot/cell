# Cell (0.0.1) 

Cell is a command line text editor like vim and emacs made using C

# Usage

`cell [filename]` 

# Operating Systems
cell only currently works on Linux and MacOS

Windows is coming soon..

# Installation
## Linux
cell is only avaliable on the AUR for now but you can install it on any distro by following the steps below:

### Clone the repo
`git clone https://gitlab.com/ShaharyarAhmed-bot/cell`

Then cd into the cloned repo

`cd cell`

### Build

To build the project you need to install qmake on your distro

for arch linux in order to install qmake you would need to run the following command

`sudo pacman -S qt5-base`

Then run

`qmake cell.pro`

You then need to copy the binary from build/bin in the project directory to the /bin folder on your linux system

After following these steps you can use cell inside your terminal

### Arch Linux
You can install cell by using your AUR helper such as yay 

`yay -S cell`


### MacOS
To run cell in MacOS you can follow the same steps mentioned in the Linux Installation guide but you would need to install qmake
using homebrew

`brew install qt`
