TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR=bin
OBJECTS_DIR=generated_files #Intermediate object files directory
MOC_DIR=generated_files #Intermediate moc files directory

SOURCES += \
        src/append_buffer.c \
        src/editor_operations.c \
        src/file_io.c \
        src/find.c \
        src/input.c \
        src/main.c \
        src/mode.c \
        src/output.c \
        src/row_operations.c \
        src/syntax_highlighting.c \
        src/terminal.c

HEADERS += \
    header/append_buffer.h \
    header/data.h \
    header/defines.h \
    header/editor_operations.h \
    header/file_io.h \
    header/filetypes.h \
    header/find.h \
    header/input.h \
    header/mode.h \
    header/output.h \
    header/row_operations.h \
    header/syntax_highlighting.h \
    header/terminal.h

DISTFILES += \
    PKGBUILD \
    README.md



