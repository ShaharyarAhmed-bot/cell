
#include <stdlib.h>
#include <string.h>

#include "header/find.h"
#include "header/input.h"
#include "header/data.h"
#include "header/defines.h"
#include "header/row_operations.h"

void editorFindCallback(char *query, int key)
{
    static int last_match = -1; // index of the row that the last match was on or -1 if no match
    static int direction = 1; // direction of the search: 1 for searching forward, and -1 for searching backward.

    static int saved_hl_line;
    static char* saved_hl = NULL;

    if (saved_hl)
    {
        memcpy(E.row[saved_hl_line].hl, saved_hl, E.row[saved_hl_line].rsize);
        free(saved_hl);
        saved_hl = NULL;
    }

    if (key == '\r' || key == '\x1b') 
    {
        last_match = -1;
        direction = 1;
        return;
    }

    else if (key == ARROW_RIGHT || key == ARROW_DOWN)
    {
        direction = 1;
    }

    else if (key == ARROW_LEFT || key == ARROW_UP)
    {
        direction = -1;
    }

    else
    {
        last_match = -1;
        direction = 1;
    }
        
    
    if (last_match == -1) direction = 1;
    int current = last_match;

    int i;
    for (i = 0; i < E.numrows; i++) 
    {
        current += direction;
        if (current == -1) current = E.numrows - 1;
        else if (current == E.numrows) current = 0;

        erow *row = &E.row[current];
        char *match = strstr(row->render, query);

        if (match) 
        {
            last_match = current;
            E.cy = current;
            E.cx = editorRowRxToCx(row, match - row->render);
            E.rowoff = E.numrows;

            saved_hl_line = current;
            saved_hl = malloc(row->size);
            memcpy(saved_hl, row->hl, row->rsize);
            memset(&row->hl[match - row->render], HL_MATCH, strlen(query));
            break;
        }
    }
}

void editorFind() 
{
    int saved_cx = E.cx;
    int saved_cy = E.cy;
    int saved_coloff = E.coloff;
    int saved_rowoff = E.rowoff;

    char *query = editorPrompt("Search: %s (Use ESC/Arrows/Enter)", editorFindCallback);

    if (query) 
    {
        free(query);
    }

    else 
    {
        E.cx = saved_cx;
        E.cy = saved_cy;
        E.coloff = saved_coloff;
        E.rowoff = saved_rowoff;
    }
}
