#include <unistd.h>

#include "header/mode.h"
#include "header/data.h"
#include "header/defines.h"


void switchinsertMode()
{
    E.mode = INSERT_MODE;
    E.mode_str = "Insert Mode";

    write(STDOUT_FILENO, "\x1b[\x35 q", 6);
}

void switchcommandMode()
{
    E.mode = COMMAND_MODE;
    E.mode_str = "Command Mode";

    write(STDOUT_FILENO, "\x1b[\x30 q", 6);

}

