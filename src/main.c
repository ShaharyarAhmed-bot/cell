#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>

#include "header/terminal.h"
#include "header/input.h"
#include "header/output.h"
#include "header/defines.h"
#include "header/data.h"
#include "header/file_io.h"

void initEditor()
{
  E.cx = 0;
  E.cy = 0;
  E.rx = 0;
  E.coloff = 0;
  E.rowoff = 0;
  E.numrows = 0;
  E.row = NULL;
  E.dirty = 0;
  E.filename = NULL;
  E.statusmsg[0] = '\0';
  E.statusmsg_time = 0;
  E.syntax = NULL;
  E.mode = COMMAND_MODE;
  E.mode_str = "Command Mode";
  
  if (getWindowSize(&E.screenrows, &E.screencols) == -1) die("getWindowSize");
  E.screenrows -= 2;
}
int main(int argc, char* argv[])
{
  
  enableRawMode();
  initEditor();

  if (argc >= 2)
  {
    editorOpen(argv[1]);
  }
  
  editorSetStatusMessage(
    "HELP: Ctrl-S = save | Ctrl-Q = quit | Ctrl-F = find");

  while (1)
  {
    editorRefreshScreen();
    editorProcessKeyPress();
  };

  return 0; 
}
