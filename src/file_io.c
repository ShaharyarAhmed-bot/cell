#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>


#include "header/file_io.h"
#include "header/terminal.h"
#include "header/data.h"
#include "header/defines.h"
#include "header/row_operations.h"
#include "header/input.h"
#include "header/output.h"
#include "header/syntax_highlighting.h"

char* editorRowsToString(int* buflen)
{
    int total_len = 0;
    int j;

    for (j = 0; j < E.numrows; j++)
        total_len += E.row[j].size  + 1;

    *buflen = total_len;

    char* buf = malloc(total_len);
    char* p = buf;

    for (j = 0; j < E.numrows; j++)
    {
        memcpy(p, E.row[j].chars, E.row[j].size);
        p += E.row[j].size;
        *p = '\n';
        p++;
    }
    
    return buf;
}

void editorOpen(char* filename)
{
    free(E.filename);
    E.filename = strdup(filename);

    editorSelectSyntaxHighlight();
    
    FILE *fp = fopen(filename, "ab+");
   
    char* line = NULL;
    size_t linecap = 0; // line capacity
    ssize_t linelen;
    
    while ((linelen = getline(&line, &linecap, fp)) != -1) // returns length of the line
    {

        while (linelen > 0 && (line[linelen - 1] == '\n' ||
                           line[linelen - 1] == '\r'))
            linelen--;
            
        editorInsertRow(E.numrows, line, linelen);
    } 

    free(line);
    fclose(fp);
    E.dirty = 0;
}

void editorSave()
{
    if (E.filename == NULL)
    {
        E.filename = editorPrompt("Save as: %s", NULL);
        if (E.filename == NULL)
        {
            editorSetStatusMessage("Save aborted");
            return;
        }
        
        editorSelectSyntaxHighlight();
    }

    int len;
    char* buf = editorRowsToString(&len);
    
    // O_CREAT if new file does not exist
    // O_RDWR open for reading and writing
    int fd = open(E.filename, O_RDWR | O_CREAT, 0644); 
    if (fd != -1)
    {
        if (ftruncate(fd, len) != -1)
        {
            if (write(fd, buf, len) == len)
            {
                close(fd);
                free(buf);
                E.dirty = 0;
                editorSetStatusMessage("%d bytes written to disk", len);
                return;
            }
        }
        close(fd);
    }
    
    free(buf);
    editorSetStatusMessage("Can't save! I/O error: %s", strerror(errno));
    //TODO
    //write to a new, temporary file, and then rename that file to the actual file 
    //the user wants to overwrite, and carefully check for errors through the whole process.
}
