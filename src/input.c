#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

//#include <stdbool.h>

#include "header/terminal.h"
#include "header/output.h"
#include "header/data.h"
#include "header/defines.h"
#include "header/editor_operations.h"
#include "header/file_io.h"
#include "header/find.h"
#include "header/mode.h"

char* editorPrompt(char* prompt, void (*callback)(char*, int))
{
    size_t bufsize = 128;
    char* buf = malloc(bufsize);

    size_t buflen = 0;
    buf[0] = '\0';

    while (1)
    {
        editorSetStatusMessage(prompt, buf);
        editorRefreshScreen();

        int c = editorReadKey();
        if (c == DEL_KEY || c == CTRL_KEY('h') || c == BACKSPACE)
        {
            if (buflen != 0) buf[--buflen] = '\0';
        }

        else if (c == '\x1b')
        {
            editorSetStatusMessage("");
            if (callback) callback(buf, c);
            free(buf);
            return NULL;
        }

        else if (c == '\r')
        {
            if (buflen != 0)
            {
                editorSetStatusMessage("");
                if (callback) callback(buf, c);
                return buf;
            }
        }

        else if (!iscntrl(c) && c < 128)
        {
            if (buflen == bufsize - 1)
            {
                bufsize *= 2;
                buf = realloc(buf, bufsize);
            }
            buf[buflen++] = c;
            buf[buflen] = '\0';
        }
        
        if (callback) callback(buf, c);
    }
}

void editorMoveCursorLeft()
{
    if (E.cx != 0)
    {
        E.cx--;
    }

    else if (E.cy > 0)
    {
        E.cy--;
        E.cx = E.row[E.cy].size; // get to the end of the previous line
    }
}

void editorMoveCursorRight(erow *row)
{
    if (row && E.cx < row->size)
    {
        E.cx++;
    }

    else if (row && E.cx == row->size)
    {
        E.cy++;
        E.cx=0; // get to the start of next line
    }
}

void editorMoveCursorUp()
{
    if (E.cy != 0)
    {
        E.cy--;
    }

}

void editorMoveCursorDown()
{
    if (E.cy < E.numrows - 1)
    {
        E.cy++;
    }
}

int editorCommandMode(int key)
{
    erow* row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
    switch (key)
    {


        case 'h':
        case ARROW_LEFT:
            editorMoveCursorLeft();

            return 1;
            break;

        case 'l':
        case ARROW_RIGHT:
            editorMoveCursorRight(row);
            return 1;
            break;

        case 'k':
        case ARROW_UP:
            editorMoveCursorUp();
            return 1;
            break;

        case 'j':
        case ARROW_DOWN:
            editorMoveCursorDown();
            return 1;
            break;

        case 'i':
            switchinsertMode();
            return 1;
            break;

    }

    return 0;
}

void editorMoveCursor(int key)
{
    erow* row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
    //bool x = E.cy >= E.numrows;
    //printf("%d", x); debugging  purpose
    switch (key)
    {
        case ARROW_LEFT:
            editorMoveCursorLeft();
            break;
        
        case ARROW_RIGHT:
            editorMoveCursorRight(row);
            break;

        case ARROW_UP:
            editorMoveCursorUp();
            break;

        case ARROW_DOWN:
            editorMoveCursorDown();
            break;
    }

    row = (E.cy >= E.numrows) ? NULL : &E.row[E.cy];
    int rowlen = row ? row->size : 0;

    if (E.cx > rowlen)
    {
        E.cx = rowlen;
    }
}
void editorProcessKeyPress()
{
    static int quit_times = CELL_QUIT_TIMES;

    int c = editorReadKey();
 
    switch(c)
    {
        case '\r':
            editorInsertNewLine();
            break;

        case CTRL_KEY('q'):
            if (E.dirty && quit_times > 0) 
            {
                editorSetStatusMessage("[WARNING] File has unsaved changes. "
                "Press Ctrl-Q %d more times to quit.", quit_times);
                quit_times--;
                return;
            }
            write(STDOUT_FILENO, "\x1b[2J", 4);
            write(STDOUT_FILENO, "\x1b[H", 3);

            exit(0);
            break;

        case CTRL_KEY('s'):
            editorSave();
            break;
        
        case HOME_KEY:
            E.cx = 0;
            break;

        case END_KEY:
            if (E.cy < E.numrows)
                E.cx = E.row[E.cy].size;
            
            break;

        case CTRL_KEY('f'):
            editorFind();
            break;

        case BACKSPACE:
        case CTRL_KEY('h'):
        case DEL_KEY:
            if (c == DEL_KEY) editorMoveCursor(ARROW_RIGHT);
            editorDelChar();
            break;
            
        case PAGE_UP:
        case PAGE_DOWN:
            {
                if (c == PAGE_UP)
                {
                    E.cy = E.rowoff;
                }

                else if (c == PAGE_DOWN)
                {
                    E.cy = E.rowoff + E.screenrows - 1;
                    if (E.cy > E.numrows) E.cy = E.numrows;
                }

                int times = E.screenrows;

                while (times--)
                    editorMoveCursor(c == PAGE_UP ? ARROW_UP : ARROW_DOWN);
                break;
            }

        //case 'h':
        //case 'j':
        //case 'k':
        //case 'l':
        case ARROW_UP:
        case ARROW_DOWN:
        case ARROW_LEFT:
        case ARROW_RIGHT:
            editorMoveCursor(c);
            break;

        case CTRL_KEY('l'):
        case 27:
            switchcommandMode();

        default:
            if (E.mode == INSERT_MODE)
            {
                editorInsertChar(c);
            }

            else
            {
                int out = editorCommandMode(c);
                if (out == 1)
                    break;

                else
                    break;
            }

            break;
    }

    quit_times = CELL_QUIT_TIMES;
    
}
