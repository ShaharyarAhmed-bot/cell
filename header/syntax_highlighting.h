#pragma once

#include "data.h"

int is_seperator(int c);
void editorUpdateSyntax(erow* row);
int editorSyntaxToColor(int hl);
void editorSelectSyntaxHighlight();