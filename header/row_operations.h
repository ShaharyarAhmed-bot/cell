#pragma once

#include <sys/types.h>

#include "data.h"

int editorRowCxToRx(erow* row, int cx); // TODO study it
int editorRowRxToCx(erow *row, int rx); 
void editorUpdateRow(erow* row);
void editorInsertRow(int at, char* s, size_t len);
void editorFreeRow(erow* row);
void editorDelRow(int at);
void editorRowInsertChar(erow* row, int at, int c);
void editorRowAppendString(erow* row, char* s, size_t len);
void editorRowDelChar(erow* row, int at);
