#pragma once

#include "append_buffer.h"

void editorScroll();

void editorDrawStatusBar(struct abuf *ab);

void editorDrawMessageBar(struct abuf *ab);

void editorDrawRows(struct abuf *ab);

void editorRefreshScreen();

void editorSetStatusMessage(const char* fmt, ...);