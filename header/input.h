#pragma once

#include "data.h"

char* editorPrompt(char* prompt, void (*callback)(char*, int));

void editorMoveCursorLeft();
void editorMoveCursorRight(erow *row);
void editorMoveCursorUp();
void editorMoveCursorDown();

int editorCommandMode(int key);
void editorMoveCursor(int key);
void editorProcessKeyPress();
