#pragma once

#include <stdlib.h>

#include "data.h"
#include "defines.h"

char* C_HL_extensions[] = {".c", ".h", NULL};
char* C_HL_keywords[] =  {
  "switch", "if", "while", "for", "break", "continue", "return", "else",
  "struct", "union", "typedef", "static", "enum", "class", "case",


  "int|", "long|", "double|", "float|", "char|", "unsigned|", "signed|",
  "void|", NULL
};

char* CPP_HL_extensions[] = {".c", ".h", NULL};

char* P_HL_extensions[] = {".py", NULL};
char* P_HL_keywords[] =  {
  "with", "if", "while", "for", "break", "continue", "return", "else", "class", "self", "def",
  "open",
 


  "import|","__init__|", "__repr__|", "__str__|", "__get__|", NULL
};

char* TXT_HL_extensions[] = {".txt", NULL};
char* TXT_HL_keywords[] =  { NULL };

struct editorSyntax HLDB[] = 
{
  {
        "txt",
        TXT_HL_extensions,
        TXT_HL_keywords,
        "//", "/*", "*/",
        HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS
    },

    {
        "c",
        C_HL_extensions,
        C_HL_keywords,
        "//", "/*", "*/",
        HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS
    },

    {
        "c++",
        C_HL_extensions,
        C_HL_keywords,
        "//", "/*", "*/",
        HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS
    },

    {
        "python",
        P_HL_extensions,
        P_HL_keywords,
        "#", "\"\"\"", "\"\"\"",
        HL_HIGHLIGHT_NUMBERS | HL_HIGHLIGHT_STRINGS
    },
};

#define HLDB_ENTRIES (sizeof(HLDB) / sizeof(HLDB[0]))
